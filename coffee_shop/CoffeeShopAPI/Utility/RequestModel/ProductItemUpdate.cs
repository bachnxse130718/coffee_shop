﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class ProductItemUpdate
    {
        public string Description { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        public string ProductImage { get; set; }

    }
}
