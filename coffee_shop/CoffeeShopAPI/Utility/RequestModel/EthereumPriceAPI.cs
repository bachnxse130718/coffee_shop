﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class EthereumPriceAPI
    {
        [JsonProperty("ethereum")]
        public Ethereum Ethereum { get; set; }
    }
    public partial class Ethereum
    {
        [JsonProperty("usd")]
        public double Usd { get; set; }
    }
}
