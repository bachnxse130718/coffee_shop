﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Enum
{
    public enum ProductStatus
    {
        Enable = 1,
        Disable = 0,
    }
}
