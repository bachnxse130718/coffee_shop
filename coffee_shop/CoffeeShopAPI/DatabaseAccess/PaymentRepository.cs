﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess
{
    public class PaymentRepository : Repository<Payment>, IPaymentRepository
    {
        private readonly CoffeeShopContext _context;

        public PaymentRepository(CoffeeShopContext context) : base(context)
        {
            _context = context;
        }


    }
}
