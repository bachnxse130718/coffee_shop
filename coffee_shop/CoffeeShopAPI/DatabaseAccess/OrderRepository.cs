﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        private readonly CoffeeShopContext _context;

        public OrderRepository(CoffeeShopContext context) : base(context)
        {
            _context = context;
        }


    }
}
