﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class TransactionReceiptAPI
    {
        public string EthereumTxHash { get; set; }
        public string TransactionHash { get; set; }
        public string Status { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public double Value { get; set; }
        public double TransactionFee { get; set; }
        public string Type { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
