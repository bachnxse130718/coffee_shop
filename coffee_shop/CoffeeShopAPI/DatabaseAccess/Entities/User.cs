﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class User
    {
        public User()
        {
            Brands = new HashSet<Brand>();
            Orders = new HashSet<Order>();
        }

        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int RoleId { get; set; }
        public bool? Status { get; set; }
        public string Token { get; set; }
        public bool EmailVerified { get; set; }
        public string EmaiVerifyToken { get; set; }
        public string ForgotPasswordToken { get; set; }
        public DateTime? CreatedDateToken { get; set; }
        public string PaymentId { get; set; }

        public virtual Payment Payment { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<Brand> Brands { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
