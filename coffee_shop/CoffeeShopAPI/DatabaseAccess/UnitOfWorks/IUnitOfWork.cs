﻿using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.UnitOfWorks
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        
        IBrandRepository BrandRepository { get; }

        IProductRepository ProductRepository { get; }
        IOrderRepository OrderRepository { get; }
        IOrderProductRepository OrderProductRepository { get; }

        IPaymentRepository PaymentRepository { get; }

        Task SaveAsync();

    }
}
