﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ResponseModel
{
    public class OrderProductListResponse
    {
        public string OrderProductId { get; set; }
        public string OrderID { get; set; }
        public string Product { get; set; }
        public int? Quantity { get; set; }
    }
}
