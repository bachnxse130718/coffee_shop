﻿using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;

namespace Services.Services
{ 
    public class JWTService
    {
        private readonly IUnitOfWork _unitOfWork;
        public JWTService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public string CreateToken(User userInfo)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("0967340455100920");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.NameIdentifier,userInfo.UserId),
                    new Claim(ClaimTypes.Email, userInfo.Email),
                    new Claim(ClaimTypes.Role, userInfo.RoleId.ToString())
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        public async Task<bool> SaveToken(string token, string userId)
        {
            bool result = false;
            try
            {
                var user = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.UserId == userId);
                if (user != null)
                {
                    user.Token = token;
                    _unitOfWork.UserRepository.Update(user);
                    await _unitOfWork.SaveAsync();
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        public async Task<User> DecodeToken(string token)
        {
            User result = null;
            try
            {
                var key = Encoding.ASCII.GetBytes("0967340455100920");
                var handler = new JwtSecurityTokenHandler();
                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    RequireExpirationTime = true,
                    ValidateLifetime = true
                };
                var tokenInfo = handler.ValidateToken(token, validations, out var securityToken);
                string id = tokenInfo.Claims.First(claim => claim.Type == ClaimTypes.NameIdentifier).Value;
                string userEmail = tokenInfo.Claims.First(claim => claim.Type == ClaimTypes.Email).Value;
                int roleId = int.Parse(tokenInfo.Claims.First(claim => claim.Type == ClaimTypes.Role).Value);
                result = new User()
                {
                    UserId = id,
                    Email = userEmail,
                    RoleId = roleId
                };
            }
            catch (Exception ex)
            {

            }
            return result;

        }
    }
}
