﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class BrandItemUpdate
    {
        public string BrandName { get; set; }
        public string BrandImage { get; set; }
        public string Description { get; set; }
    }
}
