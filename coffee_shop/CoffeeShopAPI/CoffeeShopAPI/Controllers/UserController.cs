﻿using DatabaseAccess.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System.Threading.Tasks;
using Utility.Models;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace CoffeeShopAPI.Controllers
{
    [Authorize]
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService )
        {
            _userService = userService;
        }
        [AllowAnonymous]
        [HttpPost("LoginUser")]
        public async Task<IActionResult> LoginUser([FromBody] UserAccount userAccount)
        {
            UserAccountResponse user = await _userService.CheckUserLogin(userAccount);
            if (user != null)
            {
                return Ok(user);
            }
            else
            {
                return Ok("Invalid Email or Password");
            }
        }

        [AllowAnonymous]
        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] string Email)
        {
            bool check = await _userService.ForgotPassword(Email);
            if (check)
            {
                return Ok("The email was sent successfully");
            }
            else
            {
                return Ok("Email not exist!");
            }
        }

        [AllowAnonymous]
        [HttpPost("RegisterAccount")]
        public async Task<IActionResult> RegisterAccount([FromBody] SelfProfile selfProfile)
        {
            JsonResultResponse jsonResultResponse = await _userService.RegisterAccount(selfProfile);
            return Ok(jsonResultResponse);

        }

        [AllowAnonymous]
        [HttpPost("CreateAccountManager")]
        public async Task<IActionResult> CreateAccountManager([FromBody] SelfProfile selfProfile)
        {
            JsonResultResponse jsonResultResponse = await _userService.CreateAccountManager(selfProfile);
            return Ok(jsonResultResponse);

        }

        [AllowAnonymous]
        [HttpGet("VerifyEmail")]
        public async Task<IActionResult> VerifyEmail(string verificationToken)
        {
            bool result = await _userService.VerifyEmail(verificationToken);
            if (result)
            {
                RedirectResult redirectResult = new RedirectResult("https://www.google.com/", true);
                return redirectResult;
            }
            else
            {
                return Unauthorized();
            }
        }

        [AllowAnonymous]
        [HttpGet("VerifyForgotPassword")]
        public async Task<IActionResult> VerifyForgotPassword(string forgotToken)
        {
            string result = await _userService.CheckTimeForgotPasswordToken(forgotToken);
            if (result != string.Empty)
            {
                RedirectResult redirectResult = new RedirectResult("https://www.google.com/changePassword?tk=" + result, true);
                return redirectResult;
            }
            else
            {
                return Unauthorized();
            }
        }

        //Logout
        [HttpPost("Logout")]
        public async Task<IActionResult> Logout()
        {
            var authHeader = Request.Headers["Authorization"].ToString();
            var token = authHeader.Split(' ')[1];
            var result = await _userService.Logout(token);
            if (result)
            {
                return Ok("Logout Successful");
            }
            else
            {
                return Ok("Error Occurred");
            }
        }

        [AllowAnonymous]
        //Get all users
        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers()
        {
            var result = await _userService.GetAllUsers();
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("GetAllManagers")]
        public async Task<IActionResult> GetAllManagers()
        {
            var result = await _userService.GetAllManagers();
            return Ok(result);
        }

        [AllowAnonymous]

        //Get user by id
        [HttpGet("GetDetailUserByID")]
        public async Task<IActionResult> GetDetailUserByID(string id)
        {
            var result = await _userService.GetDetailUserByID(id);
            return Ok(result);
        }

        [AllowAnonymous]
        //Get user by email
        [HttpGet("GetDetailUserByEmail")]
        public async Task<IActionResult> GetDetailUserByEmail(string email)
        {
            var result = await _userService.GetDetailUserByEmail(email);
            return Ok(result);
        }
        [AllowAnonymous]

        //Update Customer Profile
        [HttpPut("UpdateProfileCustomer")]
        public async Task<IActionResult> UpdateProfileCustomer(string id, SelfProfileUpdate selfProfile)
        {
            JsonResultResponse jsonResultResponse = await _userService.UpdateProfileCustomer(id, selfProfile);
            return Ok(jsonResultResponse);
        }

        [AllowAnonymous]

        //Update Active
        [HttpPut("UpdateActiveUser")]
        public async Task<IActionResult> UpdateActiveUser(string id, UserActive userActive)
        {
            JsonResultResponse jsonResultResponse = await _userService.UpdateActiveUser(id, userActive);
            return Ok(jsonResultResponse);
        }

    }
}
