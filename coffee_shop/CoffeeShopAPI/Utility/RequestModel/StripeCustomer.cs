﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class StripeCustomer
    {
        public string UserId { get; set; }
        public string CardId { get; set; }
    }
}
