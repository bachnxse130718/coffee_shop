﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess
{
    public class OrderProductRepository : Repository<OrderProduct>, IOrderProductRepository
    {
        private readonly CoffeeShopContext _context;

        public OrderProductRepository(CoffeeShopContext context) : base(context)
        {
            _context = context;
        }


    }
}
