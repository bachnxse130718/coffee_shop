﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ResponseModel
{
    internal class BrandListResponse
    {
        public string BrandId { get; set; }
        public string BrandName { get; set; }
    }
}
