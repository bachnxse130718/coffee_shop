﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class HistoryVoucher
    {
        public string Id { get; set; }
        public string VoucherId { get; set; }
        public string UserId { get; set; }

        public virtual User User { get; set; }
        public virtual Voucher Voucher { get; set; }
    }
}
