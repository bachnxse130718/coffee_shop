﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class Payment
    {
        public Payment()
        {
            Users = new HashSet<User>();
        }

        public string PaymentId { get; set; }
        public string StripeCustomerId { get; set; }
        public string CardId { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
