﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;
using Utility.Models;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{
    public class BrandService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BrandService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        
        public async Task<JsonResultResponse> CreateNewBrand(BrandItem brandItem)
        {
            JsonResultResponse jsonResultResponse = null;
                Brand existed = await _unitOfWork.BrandRepository.GetFirstOrDefault(
                q => q.BrandName.ToLower().Equals(brandItem.BrandName.ToLower()));
                if (existed != null)
                {
                    return jsonResultResponse = new JsonResultResponse() { success = false, message = "This brand name already exist" }; ;
                }
                else
                {
                    Brand newBrand = _mapper.Map<Brand>(brandItem);
                    newBrand.BrandId = Guid.NewGuid().ToString();
                    await _unitOfWork.BrandRepository.Add(newBrand);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Create brand success" }; ;
            }


        }

        public async Task<IEnumerable<Brand>> GetAllBrands()
        {

                return await _unitOfWork.BrandRepository.GetAll();

        }


        public async Task<IEnumerable<Brand>> GetBrandByName(string brandName)
        {

                return await _unitOfWork.BrandRepository.GetAll(
                q => q.BrandName.Contains(brandName),
                o => o.OrderBy(s => s.BrandName));

        }

        public async Task<JsonResultResponse> UpdateBrand(string id, BrandItemUpdate brandItem)
        {
            JsonResultResponse jsonResultResponse = null;
            Brand updated = await _unitOfWork.BrandRepository.GetFirstOrDefault(
            q => q.BrandId == id);
            Brand updated2 = await _unitOfWork.BrandRepository.GetFirstOrDefault(
                q => q.BrandId == id);
            if (updated != null)
            {
                updated = _mapper.Map<BrandItemUpdate, Brand>(brandItem);
                updated.BrandId = id;
                updated.UserId = updated2.UserId;
                _unitOfWork.BrandRepository.Update(updated);
                await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update brand success" }; ;
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update brand fail" }; ;

        }

        public async Task<JsonResultResponse> RemoveBrand(string id)
        {
            JsonResultResponse jsonResultResponse = null;
            Brand brand = await _unitOfWork.BrandRepository.GetFirstOrDefault(q => q.BrandId == id);
                if (brand != null)
                {
                    try
                    {
                        _unitOfWork.BrandRepository.Remove(brand);
                        await _unitOfWork.SaveAsync();
                    return jsonResultResponse = new JsonResultResponse() { success = true, message = "Delete brand success" }; ;
                    }
                     catch (Exception ex)
                    {
                    return jsonResultResponse = new JsonResultResponse() { success = true, message = "This brand can't be removed because there are still products available!" }; ;
                }
            }
            return jsonResultResponse = new JsonResultResponse() { success = true, message = "Delete brand success" }; ;

        }

    }
}
