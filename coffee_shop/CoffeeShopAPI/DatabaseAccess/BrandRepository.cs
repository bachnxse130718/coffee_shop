﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess
{
    public class BrandRepository : Repository<Brand>, IBrandRepository
    {
        private readonly CoffeeShopContext _context;

        public BrandRepository(CoffeeShopContext context) : base(context)
        {
            _context = context;
        }


    }
}
