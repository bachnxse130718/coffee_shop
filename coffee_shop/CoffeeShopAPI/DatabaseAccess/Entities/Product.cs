﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class Product
    {
        public Product()
        {
            OrderProducts = new HashSet<OrderProduct>();
        }

        public string ProductId { get; set; }
        public string BrandId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int? Amount { get; set; }
        public double? Price { get; set; }
        public string ProductImage { get; set; }
        public bool? ProductStatus { get; set; }

        public virtual Brand Brand { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
