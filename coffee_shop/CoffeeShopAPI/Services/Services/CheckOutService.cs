﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{
    public class CheckOutService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CheckOutService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<JsonResultResponse> CreateNewCheckOut(CheckOutItem checkOutItem)
        {
            var checkAmountProduct = await CheckAmountProduct(checkOutItem);
            JsonResultResponse jsonResultResponse = null;
            if (checkAmountProduct.Equals(string.Empty))
            {
                Guid getUID = Guid.NewGuid();
                string orderGetUID = getUID.ToString();
                Order newOrder = new Order()
                {
                    OrderId = orderGetUID,
                    UserId = checkOutItem.UserId,
                    TotalPrice = checkOutItem.TotalPrice,
                    OrderDate = DateTime.Now,
                    ModifiDate = DateTime.Now,
                    PaymentId = null,
                    Status = checkOutItem.Status
                };
                await _unitOfWork.OrderRepository.Add(newOrder);
                await _unitOfWork.SaveAsync();
                foreach (var item in checkOutItem.OrderProductItem)
                {
                    Product product = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                        q => q.ProductId == item.ProductId);
                    if (product.Amount >= item.Quantity)
                    {
                        if (newOrder != null)
                        {

                            Guid getUID2 = Guid.NewGuid();
                            string orderGetUID2 = getUID2.ToString();
                            OrderProduct newOrederProduct = new OrderProduct()
                            {
                                OrderProductId = orderGetUID2,
                                OrderId = orderGetUID,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity
                            };
                            product.Amount = product.Amount - item.Quantity;
                            _unitOfWork.ProductRepository.Update(product);
                            await _unitOfWork.OrderProductRepository.Add(newOrederProduct);
                            await _unitOfWork.SaveAsync();
                            return jsonResultResponse = new JsonResultResponse() { success = true, message = "Checkout success" }; ;
                        }
                    }
                    return jsonResultResponse = new JsonResultResponse() { success = false, message = "Checkout fail" }; ;

                }
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = checkAmountProduct }; ;
        }

        public async Task<string> CheckAmountProduct(CheckOutItem checkOutItem)
        {
            string result = string.Empty;
            foreach (var item in checkOutItem.OrderProductItem)
            {
                Product product = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                    q => q.ProductId == item.ProductId);
                if (product.Amount < item.Quantity)
                {
                    return result = "The number of products" + product.ProductName + "in stock is not enough";
                }
            }
            return result;
        }
    }
}