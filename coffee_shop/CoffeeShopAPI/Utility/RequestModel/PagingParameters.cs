﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class PagingParameters
    {
        const int maxPageSize = 500;
        public int PageNumber {get; set; } = 1;

        private int _pageSize = 10;
        public int Pagesize {
            get
            {
                return _pageSize;
            }
            set 
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }  
        }
    }
}
