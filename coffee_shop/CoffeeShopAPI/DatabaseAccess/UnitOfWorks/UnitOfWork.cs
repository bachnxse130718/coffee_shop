﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CoffeeShopContext _context;

        public UnitOfWork(CoffeeShopContext context)
        {
            _context = context;
            UserRepository = new UserRepository(_context);
            BrandRepository = new BrandRepository(_context);
            ProductRepository = new ProductRepository(_context);   
            OrderRepository = new OrderRepository(_context);
            OrderProductRepository = new OrderProductRepository(_context);
            PaymentRepository = new PaymentRepository(_context);

        }

        public IUserRepository UserRepository { get; private set; }
        public IBrandRepository BrandRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }
        public IOrderRepository OrderRepository { get; private set; }
        public IOrderProductRepository OrderProductRepository { get; private set; }
        public IPaymentRepository PaymentRepository { get; private set; }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
