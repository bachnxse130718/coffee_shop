﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ResponseModel
{
    public class ProductByBrandIdResponse
    {
        public string ProductId { get; set; }
        public string BrandName { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int? Amount { get; set; }
        public double? Price { get; set; }
        public string ProductImage { get; set; }
        public bool? ProductStatus { get; set; }

    }
}
