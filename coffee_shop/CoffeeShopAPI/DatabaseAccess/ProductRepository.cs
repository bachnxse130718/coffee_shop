﻿using DatabaseAccess.Entities;
using DatabaseAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess
{
    internal class ProductRepository : Repository<Product>, IProductRepository
    {
        private readonly CoffeeShopContext _context;

        public ProductRepository(CoffeeShopContext context) : base(context)
        {
            _context = context;
        }


    }
}
