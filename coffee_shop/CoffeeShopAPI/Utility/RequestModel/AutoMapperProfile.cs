﻿using AutoMapper;
using DatabaseAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Models;

namespace Utility.RequestModel
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserAccount, User>();
            CreateMap<AdminAccount, User>();
            CreateMap<ManagerAccount, User>();
            CreateMap<SelfProfileUpdate, User>();
            CreateMap<SelfProfile, User>();
            CreateMap<UserActive, User>();
            CreateMap<BrandItem, Brand>();
            CreateMap<BrandItemUpdate, Brand>();
            CreateMap<ProductItem, Product>();
            CreateMap<ProductItemUpdate, Product>();
            CreateMap<OrderItem, Order>();
            CreateMap<OrderStatusUpdate, Order>();
            CreateMap<OrderProductItem, OrderProduct>();






        }
    }
}
