﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class Order
    {
        public Order()
        {
            OrderProducts = new HashSet<OrderProduct>();
        }

        public string OrderId { get; set; }
        public string UserId { get; set; }
        public string PaymentId { get; set; }
        public double? TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ModifiDate { get; set; }
        public int? Status { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
