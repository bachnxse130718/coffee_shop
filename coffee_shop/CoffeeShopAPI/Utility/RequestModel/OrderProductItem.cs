﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class OrderProductItem
    {
        public string ProductId { get; set; }
        public int? Quantity { get; set; }
    }
}
