﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System.Threading.Tasks;
using Utility.RequestModel;

namespace CoffeeShopAPI.Controllers
{
    [Authorize]
    [Route("api/orderproduct")]
    [ApiController]
    public class OrderProductController : ControllerBase
    {
         private readonly OrderProductService _orderProductSevice;

            public OrderProductController(OrderProductService orderProductService)
            {
                _orderProductSevice = orderProductService;
            }
            [AllowAnonymous]
            [HttpPost("CreateNewOrderProduct")]
            public async Task<IActionResult> CreateNewOrderProduct(OrderProductItem orderProductItem)
            {
                bool check = await _orderProductSevice.CreateNewOrderProduct(orderProductItem);
                if (check)
                {
                    return Ok("Create new order product successfully!");
                }
                else
                {
                    return Ok("Create new order product fail!");
                }
            }


            [AllowAnonymous]
            [HttpGet("GetOrderProductByOrderId")]
            public async Task<IActionResult> GetOrderProductByOrderId(string id)
            {
                var result = await _orderProductSevice.GetOrderProductByOrderId(id);
                return Ok(result);
            }
        }
    
}
