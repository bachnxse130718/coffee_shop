﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace CoffeeShopAPI.Controllers
{
    [Authorize]
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly OrderService _orderSevice;

        public OrderController(OrderService orderService)
        {
            _orderSevice = orderService;
        }

        [AllowAnonymous]
        [HttpPut("UpdateOrderStatus")]
        public async Task<IActionResult> UpdateOrderStatus(string id, OrderStatusUpdate orderStatus)
        {
            JsonResultResponse jsonResultResponse = await _orderSevice.UpdateOrderStatus(id, orderStatus);
            return Ok(jsonResultResponse);
        }
        
        
        [AllowAnonymous]
        [HttpGet("GetOrderByUserId")]
        public async Task<IActionResult> GetOrderByUserId(string id)
        {
            var result = await _orderSevice.GetOrderByUserId(id);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("GetAllOrder")]
        public async Task<IActionResult> GetAllOrder()
        {
            var result = await _orderSevice.GetAllOrder();
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("ChartOrder")]
        public async Task<IActionResult> ChartOrder(string id,string id2)
        {
            var result = await _orderSevice.ChartOrder(id,id2);
            return Ok(result);
        }
    }
}
