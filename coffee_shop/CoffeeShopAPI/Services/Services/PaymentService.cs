﻿using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Utility.RequestModel;

namespace Services.Services
{
    public class PaymentService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserService _userService;

        private readonly string apiKey = "sk_test_51GrzQiEZrqTcMYrJCaxFk6lQZv9zWRYnf33keblkZ9Km8q6mNHZhxilV93eCFZT0BbNmobnDKM9nfR9hpPfLEvdu00X938VgNv";
        public PaymentService(UnitOfWork unitOfWork, UserService userService)
        {
            _unitOfWork =  unitOfWork;
            _userService = userService;
        }


        public async Task<bool> AddCard(string token, CardAPI cardInfo)
        {
            bool result = false;
            try
            {

                var checkedToken = await _userService.CheckToken(token);
                if (checkedToken != null)
                {
                    if (checkedToken.PaymentId == null)
                    {
                        //Create Customer
                        var createCustomerResult = await CreateCustomerAsync(checkedToken, cardInfo);
                        if (!createCustomerResult.Equals(string.Empty))
                        {
                            //Save Stripe Customer Id to DB for later use
                            Payment newPayment = new Payment()
                            {
                                PaymentId = Guid.NewGuid().ToString(),
                                StripeCustomerId = createCustomerResult.UserId,
                                CardId = createCustomerResult.CardId

                            };
                            await _unitOfWork.PaymentRepository.Add(newPayment);
                            checkedToken.PaymentId = newPayment.PaymentId;
                            _unitOfWork.UserRepository.Update(checkedToken);
                            await _unitOfWork.SaveAsync();
                            return true;

                        }

                    }

                }


            }
            catch (Exception ex)
            {

            }
            return result;
        }

        private async Task<StripeCustomer> CreateCustomerAsync(User userInfo, CardAPI cardInfo)
        {
            StripeCustomer result = null;
            try
            {
                Stripe.StripeConfiguration.ApiKey = apiKey;
                var newToken = await CreateTokenAsync(userInfo, cardInfo);
                if (newToken != null)
                {
                    Stripe.CustomerCreateOptions myCustomer = new Stripe.CustomerCreateOptions();
                    myCustomer.Email = userInfo.Email;
                    myCustomer.Source = newToken.TokenId;
                    var customerService = new Stripe.CustomerService();
                    Stripe.Customer stripeCustomer = await customerService.CreateAsync(myCustomer);
                    if (stripeCustomer.StripeResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        result = new StripeCustomer()
                        {
                            CardId = newToken.CardId,
                            UserId = stripeCustomer.Id
                        };

                    }
                }


            }
            catch (Exception ex)
            {

            }
            return result;
        }

        private async Task<TokenAPI> CreateTokenAsync(User userInfo, CardAPI cardInfo)
        {
            TokenAPI result = null;
            try
            {
                Stripe.StripeConfiguration.ApiKey = apiKey;
                //Create card
                Stripe.CreditCardOptions card = new Stripe.CreditCardOptions();
                card.Name = cardInfo.Name;
                card.Number = cardInfo.Number;
                card.ExpMonth = cardInfo.ExpMonth;
                card.ExpYear = cardInfo.ExpYear;
                card.Cvc = cardInfo.CVC;
                card.Currency = "usd";
                //Create Token
                Stripe.TokenCreateOptions stripeToken = new Stripe.TokenCreateOptions();
                stripeToken.Card = card;
                Stripe.TokenService serviceToken = new Stripe.TokenService();
                Stripe.Token newToken = await serviceToken.CreateAsync(stripeToken);
                if (newToken.StripeResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = new TokenAPI()
                    {
                        TokenId = newToken.Id,
                        CardId = newToken.Card.Id

                    };

                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<TransactionReceiptAPI> Deposit(string token, double amount)
        {
            TransactionReceiptAPI result = null;
            try
            {
                //Check Token 
                var checkedToken = await _userService.CheckToken(token);
                if (checkedToken != null)
                {
                    //get payment info
                    var userPayment = await _unitOfWork.PaymentRepository.GetFirstOrDefault(q => q.PaymentId == checkedToken.PaymentId);
                    if (userPayment != null )
                    {

                        var fee = Math.Round((await CalculateFeeAsync()) + (amount * 0.029 + 0.3));
                        if (amount > fee)
                        {

                            double amountAfterFee = amount - fee;

                            //Charge Amount From User's Card
                            //var stripeCharge = await ChargeUser(checkedToken.Email, userPayment.StripeCustomerId, amount);
                            //if (stripeCharge)
                            //{
                            //    //Deposit
                            //    var depositResult = await _ethereum.DepositAsync(checkedToken.WalletId, amountAfterFee);
                            //    if (depositResult)
                            //    {
                            //        //Create Transaction 
                            //        var trsHash = _hash.SHA256(checkedToken.Id + DateTime.Now);
                            //        var createdTrs = await _ethereum.CreateTransaction(trsHash, checkedToken.WalletId, systemWalletId, 1, amountAfterFee, fee);

                            //        if (!createdTrs.Equals(string.Empty))
                            //        {
                                        // Save Transaction
                                        //TransactionsHistory history = new TransactionsHistory()
                                        //{
                                        //    Id = _hash.SHA256(checkedToken.Id + DateTime.Now + trsHash),
                                        //    TrsId = trsHash,
                                        //    TxHash = createdTrs,
                                        //    UserId = checkedToken.Id,
                                        //    TxType = 1
                                        //};
                                        //await _context.TransactionsHistory.AddAsync(history);
                                        //await _context.SaveChangesAsync();

                                        //result = new TransactionReceiptAPI()
                                        //{
                                        //    EthereumTxHash = createdTrs,
                                        //    From = checkedToken.WalletId,
                                        //    Status = "Succeeded",
                                        //    TimeStamp = DateTime.Now,
                                        //    To = systemWalletId,
                                        //    TransactionFee = fee,
                                        //    TransactionHash = trsHash,
                                        //    Type = "Deposit",
                                        //    Value = amountAfterFee

                                        //};
                                        //await _mail.SendMailReceipt(result, checkedToken);
                            //        }


                            //    }



                            //}


                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        private async Task<double> CalculateFeeAsync()
        {
            double result = 0;
            try
            {
                //Get ETH price 
                var ethPrice = await GetETHPriceAsync();
                result = Math.Round(ethPrice * 0.0042 * 2);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<bool> ChargeUser(string email, string stripeCustomerId, double amount)
        {
            bool result = false;
            try
            {
                Stripe.StripeConfiguration.ApiKey = apiKey;
                var options = new Stripe.ChargeCreateOptions()
                {
                    Amount = (long)amount * 100,
                    Currency = "usd",
                    Customer = stripeCustomerId,
                    ReceiptEmail = email

                };

                var service = new Stripe.ChargeService();
                Stripe.Charge charge = await service.CreateAsync(options);
                if (charge.Status.Equals("succeeded"))
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        private async Task<double> GetETHPriceAsync()
        {
            double result = 0;
            try
            {
                var httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri("https://api.coingecko.com/api/v3/");
                var respone = await httpClient.GetAsync("simple/price?ids=ethereum&vs_currencies=usd");
                if (respone.IsSuccessStatusCode)
                {
                    var content = await respone.Content.ReadAsStringAsync();
                    var price = JsonConvert.DeserializeObject<EthereumPriceAPI>(content);
                    result = price.Ethereum.Usd;
                }


            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
