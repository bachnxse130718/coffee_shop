﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;
using Utility.RequestModel;

namespace Services.Services
{
    public class MailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly MailSetting _mailSettings;
        private readonly string fromAddress = "tantnse140290@fpt.edu.vn";
        private readonly string password = "Tan0967340455";
        private readonly IMapper _mapper;
        private readonly string baseUrl = "https://api-coffeeshop.cosplane.asia/api/user/VerifyEmail?verificationToken=";
        private readonly string baseUrl2 = "https://api-coffeeshop.cosplane.asia/api/user/VerifyForgotPassword?forgotToken=";
        //private readonly string baseUrl = "https://api-coffeeshop.cosplane.asia/api/user/VerifyEmail?verificationToken=";
        //private readonly string baseUrl2 = "https://localhost:44329/";
        private readonly IHostingEnvironment _host;
        private readonly string supportUrl = "";
        public MailService(IUnitOfWork unitOfWork, IHostingEnvironment host, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _host = host;
            _mapper = mapper;
        }

        public async Task<bool> SendMailVerificationAsync(string toEmail, string fullName, string verificationToken)
        {
            var result = false;
            try
            {
                //var verificationToken = await GenerateEmailVerificationTokenAsync(toEmail);
                //if (!verificationToken.Equals(string.Empty))
                //{
                    var subject = "Verify your account";
                    var htmlContent = RenderConfirmedHTMLBody(fullName, baseUrl + verificationToken, supportUrl);

                    var message = new MimeMessage();
                    message.From.Add(new MailboxAddress("CoffeeShop", fromAddress));
                    message.To.Add(new MailboxAddress(fullName, toEmail));

                    message.Subject = subject;

                    message.Body = new TextPart("html")
                    {
                        Text = htmlContent
                    };

                    using (var smtp = new MailKit.Net.Smtp.SmtpClient())
                    {
                        await smtp.ConnectAsync("smtp.gmail.com", 587, false);
                        smtp.AuthenticationMechanisms.Remove("XOAUTH2");
                        await smtp.AuthenticateAsync(fromAddress, password);
                        await smtp.SendAsync(message);
                        await smtp.DisconnectAsync(true);
                        result = true;
                    }
                //}
            }
            catch (Exception ex)
            {

            }
            return result;
           

        }

        //private async Task<string> GenerateEmailVerificationTokenAsync(string email)
        //{
        //    string result = string.Empty;
        //    try
        //    {
        //        Guid getUID = Guid.NewGuid();
        //        string verificationToken = getUID.ToString();
        //        var user2 = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.Email == email);
        //        if (user2 != null)
        //        {
        //            user2.EmaiVerifyToken = verificationToken;
        //            _unitOfWork.UserRepository.Update(user2);
        //            await _unitOfWork.SaveAsync();
        //            result = verificationToken;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return result;
        //}





        private string RenderConfirmedHTMLBody(string firstName, string url, string supportUrl)
        {
            string body = string.Empty;
            try
            {
                string foldername = "EmailTemplates\\";
                string webRootPath = _host.ContentRootPath;
                string path = Path.Combine(webRootPath, foldername);

                using (StreamReader reader = new StreamReader(path + "EmailConfirmation.html"))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{FirstName}", firstName);
                body = body.Replace("{URL}", url);
                body = body.Replace("{SupportURL}", supportUrl);
            }
            catch (Exception ex)
            {

            }
            return body;

        }

        public async Task<bool> SendMailForgotPassword(string toEmail)
        {
            var result = false;
            try
            {
                var forgotPasswordToken = await CreateChangePasswordToken(toEmail);
                if (!forgotPasswordToken.Equals(string.Empty))
                {
                    var subject = "Change your password";
                    var htmlContent = RenderConfirmedHTMLBody2(toEmail, baseUrl2 + forgotPasswordToken, supportUrl);

                    var message = new MimeMessage();
                    message.From.Add(new MailboxAddress("CoffeeShop", fromAddress));
                    message.To.Add(new MailboxAddress(toEmail, toEmail));

                    message.Subject = subject;

                    message.Body = new TextPart("html")
                    {
                        Text = htmlContent
                    };

                    using (var smtp = new MailKit.Net.Smtp.SmtpClient())
                    {
                        await smtp.ConnectAsync("smtp.gmail.com", 587, false);
                        smtp.AuthenticationMechanisms.Remove("XOAUTH2");
                        await smtp.AuthenticateAsync(fromAddress, password);
                        await smtp.SendAsync(message);
                        await smtp.DisconnectAsync(true);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<string> CreateChangePasswordToken(string email)
        {
            string result = string.Empty;
            try
            {
                Guid getUID = Guid.NewGuid();
                string changePasswordToken = getUID.ToString();
                var user = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.Email == email);
                if (user != null)
                {
                    user.ForgotPasswordToken = changePasswordToken;
                    user.CreatedDateToken = DateTime.Now;
                    _unitOfWork.UserRepository.Update(user);
                    await _unitOfWork.SaveAsync();
                    result = changePasswordToken;
                }

            }
            catch (Exception ex)
            {

            }
            return result;
        }





        private string RenderConfirmedHTMLBody2(string firstName, string url, string supportUrl)
        {
            string body = string.Empty;
            try
            {
                string foldername = "EmailTemplates\\";
                string webRootPath = _host.ContentRootPath;
                string path = Path.Combine(webRootPath, foldername);

                using (StreamReader reader = new StreamReader(path + "EmailForgotPassword.html"))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{FirstName}", firstName);
                body = body.Replace("{URL}", url);
                body = body.Replace("{SupportURL}", supportUrl);
            }
            catch (Exception ex)
            {

            }
            return body;

        }
    }
}
