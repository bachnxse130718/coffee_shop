﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System.Threading.Tasks;
using Utility.Models;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace CoffeeShopAPI.Controllers
{
    [Route("api/brand")]
    [ApiController]
    public class BrandController : ControllerBase
    {
        private readonly BrandService _brandService;
        public BrandController(BrandService brandService)
        {
            _brandService = brandService;
        }

        //Create new brand
        [AllowAnonymous]
        [HttpPost("CreateNewBrand")]
        public async Task<IActionResult> CreateNewBrand(BrandItem brandItem)
        {
            JsonResultResponse jsonResultResponse = await _brandService.CreateNewBrand(brandItem);
            return Ok(jsonResultResponse);
        }

        //Get all brands
        [AllowAnonymous]
        [HttpGet("GetAllBrands")]
        public async Task<IActionResult> GetAllBrands()
        {
            var result = await _brandService.GetAllBrands();
            return Ok(result);
        }


        //Get brand by name
        [AllowAnonymous]
        [HttpGet("GetBrandByName")]
        public async Task<IActionResult> GetBrandByName(string brandName)
        {
            var result = await _brandService.GetBrandByName(brandName);
            return Ok(result);
        }

        [AllowAnonymous]
        //Update brand
        [HttpPut("UpdateBrand")]
        public async Task<IActionResult> UpdateBrand(string id, BrandItemUpdate brandItem)
        {
            JsonResultResponse jsonResultResponse = await _brandService.UpdateBrand(id, brandItem);
            return Ok(jsonResultResponse);
        }

        //Remove brand
        [AllowAnonymous]
        [HttpDelete("RemoveBrand")]
        public async Task<IActionResult> RemoveBrand(string id)
        {
            JsonResultResponse jsonResultResponse = await _brandService.RemoveBrand(id);
            return Ok(jsonResultResponse);
        }
    }
}
