﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class CardItem
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public int ExpYear { get; set; }
        public int ExpMonth { get; set; }
        public string CVC { get; set; }
        public string Brand { get; set; }
    }
}
