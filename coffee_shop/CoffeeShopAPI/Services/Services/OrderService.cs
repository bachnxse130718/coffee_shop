﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{

    public class OrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        

        public async Task<JsonResultResponse> UpdateOrderStatus(string id, OrderStatusUpdate orderStatus)
        {
            JsonResultResponse jsonResultResponse = null;

            Order order = await _unitOfWork.OrderRepository.GetFirstOrDefault(
                q => q.OrderId == id);
                Order order2 = await _unitOfWork.OrderRepository.GetFirstOrDefault(
                    q => q.OrderId == id);
                if (order != null)
                {
                    order = _mapper.Map<OrderStatusUpdate, Order>(orderStatus);
                    order.OrderId = id;
                    order.UserId = order2.UserId;
                    order.PaymentId = order2.PaymentId;
                    order.TotalPrice = order2.TotalPrice;
                    order.OrderDate = order2.OrderDate;
                    order.ModifiDate = order2.ModifiDate;
                    _unitOfWork.OrderRepository.Update(order);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update status success" };
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update status fail" };

        }


        public async Task<IEnumerable<Order>> ChartOrder(string dateStart, string dateEnd)
        {
            if (dateStart != null && dateEnd != null)
            {
                return await _unitOfWork.OrderRepository.GetAll(q => (q.OrderDate) >= DateTime.Parse(dateStart) && (q.OrderDate) <= DateTime.Parse(dateEnd));

            }
            else
            {
                return await _unitOfWork.OrderRepository.GetAll(q => q.OrderDate.Month == DateTime.Now.Month);

            }
            
        }




        public async Task<IEnumerable<OrderListResponse>> GetOrderByUserId(string id)
        {
            List<Order> listDB = (await _unitOfWork.OrderRepository.GetAll(
                q => q.OrderId.Contains(id))).ToList();
            List<OrderListResponse> listResponse = new List<OrderListResponse>();
            foreach (var product in listDB)
            {
                var creator = await _unitOfWork.UserRepository.GetFirstOrDefault(
                    q => q.UserId == product.UserId);
                listResponse.Add(new OrderListResponse()
                {
                    OrderId = product.OrderId,
                    User = creator.Email,
                    PaymentId = product.PaymentId,
                    TotalPrice = product.TotalPrice,
                    OrderDate = product.OrderDate,
                    ModifiDate = product.ModifiDate,
                    Status = product.Status,

                });
            }
            return listResponse;

        }


        public async Task<IEnumerable<OrderListResponse>> GetAllOrder()
        {
            List<Order> listDB = (await _unitOfWork.OrderRepository.GetAll( )).ToList();
            List<OrderListResponse> listResponse = new List<OrderListResponse>();
            foreach (var product in listDB)
            {
                var creator = await _unitOfWork.UserRepository.GetFirstOrDefault(
                    q => q.UserId == product.UserId);
                listResponse.Add(new OrderListResponse()
                {
                    OrderId = product.OrderId,
                    User = creator.Email,
                    PaymentId = product.PaymentId,
                    TotalPrice = product.TotalPrice,
                    OrderDate = product.OrderDate,
                    ModifiDate = product.ModifiDate,
                    Status = product.Status,

                });
            }
            return listResponse;

        }
    }
}
