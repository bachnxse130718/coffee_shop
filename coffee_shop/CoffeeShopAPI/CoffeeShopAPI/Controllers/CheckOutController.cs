﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace CoffeeShopAPI.Controllers
{
    [Authorize]
    [Route("api/checkout")]
    [ApiController]
    public class CheckOutController : ControllerBase
    {
        private readonly CheckOutService _checkOutService;
        public CheckOutController(CheckOutService checkOutService)
        {
            _checkOutService = checkOutService;
        }


        [AllowAnonymous]
        [HttpPost("CreateNewOrder")]
        public async Task<IActionResult> CreateNewOrder(CheckOutItem checkOutItem)
        {
            JsonResultResponse jsonResultResponse = await _checkOutService.CreateNewCheckOut(checkOutItem);
            return Ok(jsonResultResponse);
        }
    }
}
