﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class CheckOutItem
    {
        public string UserId { get; set; }
        public double? TotalPrice { get; set; }
        public string OrderDate { get; set; }
        public string ModifiDate { get; set; }
        public int? Status { get; set; }
        public List<OrderProductItem> OrderProductItem { get; set; }
    }
}
