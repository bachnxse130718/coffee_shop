﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{
    public class OrderProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public OrderProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateNewOrderProduct(OrderProductItem orderProductItem)
        {
            Product product = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                q => q.ProductId == orderProductItem.ProductId);

                OrderProduct newOrederProduct = _mapper.Map<OrderProduct>(orderProductItem);
                Guid getUID = Guid.NewGuid();
                string orderGetUID = getUID.ToString();
                newOrederProduct.OrderProductId = orderGetUID;
                await _unitOfWork.OrderProductRepository.Add(newOrederProduct);
                if(product != null)
                {
                    product.Amount = product.Amount - orderProductItem.Quantity;
                    _unitOfWork.ProductRepository.Update(product);
                }
                await _unitOfWork.SaveAsync();
                return true;

        }

        public async Task<IEnumerable<OrderProductListResponse>> GetOrderProductByOrderId(string id)
        {
            List<OrderProduct> listDB = (await _unitOfWork.OrderProductRepository.GetAll(
                q => q.OrderId.Contains(id))).ToList();
            List<OrderProductListResponse> listResponse = new List<OrderProductListResponse>();
            //For custom
            foreach (var orderProduct in listDB)
            {
                var creator = await _unitOfWork.OrderProductRepository.GetFirstOrDefault(
                    q => q.ProductId == orderProduct.ProductId,"Product");
                listResponse.Add(new OrderProductListResponse()
                {
                    OrderProductId = orderProduct.OrderProductId,
                    OrderID = orderProduct.OrderId,
                    Product = creator.Product.ProductName,
                    Quantity = orderProduct.Quantity
                });
            }
            return listResponse;
        }
    }
}

