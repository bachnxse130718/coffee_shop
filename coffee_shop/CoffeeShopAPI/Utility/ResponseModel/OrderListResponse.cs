﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.ResponseModel
{
    public class OrderListResponse
    {
        public string OrderId { get; set; }
        public string User { get; set; }
        public string PaymentId { get; set; }
        public double? TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ModifiDate { get; set; }
        public int? Status { get; set; }
    }
}
