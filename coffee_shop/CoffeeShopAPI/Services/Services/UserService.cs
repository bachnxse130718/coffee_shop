﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;
using Utility.Models;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{

    public class UserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly JWTService _jwtService;
        private readonly MailService _mailService;


        public UserService(IUnitOfWork unitOfWork, IMapper mapper, JWTService jwtService, MailService mailService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _jwtService = jwtService;
            _mailService = mailService;
        }

        public async Task<UserAccountResponse> CheckUserLogin(UserAccount userAccount)
        {
            UserAccountResponse userAccountResponse = null;
            User loginedUser = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.Email == userAccount.Email && q.Password == userAccount.Password 
                    && q.Status == true && q.EmailVerified == true, "Role");
            if (loginedUser != null)
            {
                string jwtToken = _jwtService.CreateToken(loginedUser);
                if (jwtToken != null)
                {
                    bool savedToken = await _jwtService.SaveToken(jwtToken, loginedUser.UserId);
                    if (savedToken)
                    {
                        userAccountResponse = new UserAccountResponse()
                        {
                            Email = loginedUser.Email,
                            FullName = loginedUser.FullName,
                            Address = loginedUser.Address,
                            Phone = loginedUser.Phone,
                            Token = jwtToken,
                            Role = loginedUser.Role.RoleName
                        };
                    }
                }
            }
            return userAccountResponse;
        }

        public async Task<bool> VerifyEmail(string verificationToken)
        {
            bool result = false;
            try
            {
                var checkedToken = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.EmaiVerifyToken == verificationToken);
                if (checkedToken != null)
                {
                    checkedToken.EmailVerified = true;
                    checkedToken.EmaiVerifyToken = null;
                    _unitOfWork.UserRepository.Update(checkedToken);
                    await _unitOfWork.SaveAsync();
                    result = true;

                }

            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<JsonResultResponse> RegisterAccount(SelfProfile selfProfile)
        {
            JsonResultResponse jsonResultResponse = null;
            User existed = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.Email == selfProfile.Email, "Role");
            if (existed != null)
            {
                return jsonResultResponse = new JsonResultResponse() { success = false, message = "Email already exist" }; ;
            }
            else
            {
                User newUser = _mapper.Map<User>(selfProfile);
                Guid getUID = Guid.NewGuid();
                string customerGetUID = getUID.ToString();
                newUser.UserId = customerGetUID;
                newUser.RoleId = (int)UserRole.Customer;
                newUser.Status = true;
                newUser.EmaiVerifyToken = Guid.NewGuid().ToString();
                newUser.EmailVerified = false;
                await _unitOfWork.UserRepository.Add(newUser);
                await _unitOfWork.SaveAsync();
                var sentMail = await _mailService.SendMailVerificationAsync(selfProfile.Email, selfProfile.FullName, newUser.EmaiVerifyToken);
                if (sentMail)
                {
                    return jsonResultResponse = new JsonResultResponse() { success = true, message = "Sent mail success" }; ;
                }
                return jsonResultResponse = new JsonResultResponse() { success = false, message = "Sent mail fail" }; ;
            }
        }

        public async Task<JsonResultResponse> CreateAccountManager(SelfProfile selfProfile)
        {
            JsonResultResponse jsonResultResponse = null;
            User existed = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.Email == selfProfile.Email, "Role");
            if (existed != null)
            {
                return jsonResultResponse = new JsonResultResponse() { success = false, message = "Email already exist" }; ;
            }
            else
            {
                User newUser = _mapper.Map<User>(selfProfile);
                Guid getUID = Guid.NewGuid();
                string customerGetUID = getUID.ToString();
                newUser.UserId = customerGetUID;
                newUser.RoleId = (int)UserRole.Manager;
                newUser.Status = true;
                newUser.EmaiVerifyToken = null;
                newUser.EmailVerified = true;
                await _unitOfWork.UserRepository.Add(newUser);
                await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Create account manager success" }; ;
            }
        }

        public async Task<bool> ForgotPassword(string Email)
        {
            User existed = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.Email == Email);
            if (existed == null)
            {
                return false;
            }
            else
            {
                var sentMail = await _mailService.SendMailForgotPassword(Email);
                if (sentMail)
                {
                    return true;
                }
                return false;
            }
        }


        public async Task<User> CheckToken(string token)
        {
            User result = null;
            try
            {
                var decodedToken = await _jwtService.DecodeToken(token);
                if (decodedToken != null)
                {
                    var checkedToken = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.UserId == decodedToken.UserId && q.Token == token);
                    if (checkedToken != null)
                    {
                            result = checkedToken;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public async Task<string> CheckTimeForgotPasswordToken(string forgotPasswordToken)
        {
            string result = string.Empty;
            try
            {
                var checkedToken = await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Status == true && q.ForgotPasswordToken == forgotPasswordToken &&  (DateTime.Now - (DateTime)q.CreatedDateToken).Minutes <= 20);
                if (checkedToken != null)
                {
                    string forgotToken = Guid.NewGuid().ToString();
                    checkedToken.ForgotPasswordToken = forgotToken;
                    _unitOfWork.UserRepository.Update(checkedToken);
                    await _unitOfWork.SaveAsync();
                    result = forgotToken;
                }

            }
            catch (Exception ex)
            {
            }
            return result;
        }


        public async Task<bool> Logout(string token)
        {
            bool result = false;
            try
            {
                var checkedToken = await CheckToken(token);
                if (checkedToken != null)
                {
                    checkedToken.Token = null;
                    _unitOfWork.UserRepository.Update(checkedToken);
                    await _unitOfWork.SaveAsync();
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }


        public async Task<IEnumerable<User>> GetAllUsers()
        {
                return await _unitOfWork.UserRepository.GetAll();
        }
        public async Task<IEnumerable<User>> GetAllAdmins(/*string token*/)
        {
            //var checkedToken = await CheckToken(token);
            //if (checkedToken != null)
            //{
                return await _unitOfWork.UserRepository.GetAll(q => q.RoleId == (int)UserRole.Admin,
                                   o => o.OrderBy(s => s.FullName));
            //}
            //return null;
        }
        public async Task<IEnumerable<User>> GetAllManagers()
        {
                return await _unitOfWork.UserRepository.GetAll(q => q.RoleId == (int)UserRole.Manager,
                                     o => o.OrderBy(s => s.FullName));

        }

        public async Task<User> GetDetailUserByID(string id)
        {

                return await _unitOfWork.UserRepository.Get(id);


        }

        public async Task<User> GetDetailUserByEmail(string email)
        {
                return await _unitOfWork.UserRepository.GetFirstOrDefault(q => q.Email == email);

        }

        public async Task<JsonResultResponse> UpdateProfileCustomer(string id, SelfProfileUpdate selfProfile)
        {
            JsonResultResponse jsonResultResponse = null;
            User updated = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.UserId == id);
                User updated2 = await _unitOfWork.UserRepository.GetFirstOrDefault(
                    q => q.UserId == id);
                if (updated != null)
                {
                    updated = _mapper.Map<SelfProfileUpdate, User>(selfProfile);
                    updated.UserId = id;
                    updated.Email = updated2.Email;
                    updated.Password = updated2.Password;
                    updated.RoleId = updated2.RoleId;
                    updated.Status = updated2.Status;
                    _unitOfWork.UserRepository.Update(updated);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update profile success" }; ;
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update profile fail" }; ;
        }

        public async Task<JsonResultResponse> UpdateActiveUser(string id,  UserActive userActive)
        {
            JsonResultResponse jsonResultResponse = null;

            User updated = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.UserId == id);
                User updated2 = await _unitOfWork.UserRepository.GetFirstOrDefault(
                    q => q.UserId == id);
                if (updated != null)
                {
                    updated = _mapper.Map<UserActive, User>(userActive);
                    updated.UserId = id;
                    updated.Email = updated2.Email;
                    updated.Password = updated2.Password;
                    updated.RoleId = updated2.RoleId;
                    updated.Phone = updated2.Phone;
                    updated.FullName = updated2.FullName;
                    updated.Address = updated2.Address;
                    _unitOfWork.UserRepository.Update(updated);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update active success" }; ;
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update active fail" }; ;

        }

        public async Task<JsonResultResponse> UpdatePasswordCustomer(string Email,UserUpdatePassword userUpdatePassword)
        {
            JsonResultResponse jsonResultResponse = null;

            User updated = await _unitOfWork.UserRepository.GetFirstOrDefault(
                q => q.Email == Email);
                User updated2 = await _unitOfWork.UserRepository.GetFirstOrDefault(
                    q => q.Email == Email);
                if (updated != null)
                {
                    updated = _mapper.Map<UserUpdatePassword, User>(userUpdatePassword);
                    updated.UserId = updated2.UserId;
                    updated.Email = Email;
                    updated.RoleId = updated2.RoleId;
                    updated.Phone = updated2.Phone;
                    updated.FullName = updated2.FullName;
                    updated.Address = updated2.Address;
                    updated.Status = updated2.Status;
                    _unitOfWork.UserRepository.Update(updated);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update password success" }; ;
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update password fail" }; ;
        }
    }
}
