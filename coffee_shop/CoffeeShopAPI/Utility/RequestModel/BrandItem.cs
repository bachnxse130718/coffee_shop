﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;

namespace Utility.Models
{
    public class BrandItem
    {

        public string BrandName { get; set; }
        public string BrandImage { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }


    }
}
