﻿using AutoMapper;
using DatabaseAccess.Entities;
using DatabaseAccess.UnitOfWorks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Enum;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace Services.Services
{
    public class ProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<JsonResultResponse> CreateNewProduct(ProductItem productItem)
        {
            JsonResultResponse jsonResultResponse = null;
            Product existed = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                q => q.ProductName.ToLower().Equals(productItem.ProductName.ToLower()) && q.BrandId.ToLower().Equals(productItem.BrandId.ToLower()));
                if (existed != null)
                {
                    return jsonResultResponse = new JsonResultResponse() { success = false, message = "Product in this brand already exist"};
                    }
                else
                {
                    Product newProduct = _mapper.Map<Product>(productItem);
                    newProduct.ProductId = Guid.NewGuid().ToString();
                    newProduct.ProductStatus = true;
                    await _unitOfWork.ProductRepository.Add(newProduct);
                    await _unitOfWork.SaveAsync();
                    return jsonResultResponse = new JsonResultResponse() { success = true, message = "Create product success" };
            }

        }

        public async Task<JsonResultResponse> UpdateProduct(string id, ProductItemUpdate productItem)
        {
            JsonResultResponse jsonResultResponse = null;

            Product updated = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                q => q.ProductId == id);
                Product updated2 = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                    q => q.ProductId == id);
                if (updated != null)
                {
                    updated = _mapper.Map<ProductItemUpdate, Product>(productItem);
                    updated.ProductId = id;
                    updated.BrandId = updated2.BrandId;
                    updated.ProductName = updated2.ProductName;
                    updated.ProductStatus = updated2.ProductStatus;
                    _unitOfWork.ProductRepository.Update(updated);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Update product success" };
            }
            return jsonResultResponse = new JsonResultResponse() { success = false, message = "Update product fail" };

        }



        public async Task<JsonResultResponse> RemoveProduct(string id)
        {
            JsonResultResponse jsonResultResponse = null;
            Product product = await _unitOfWork.ProductRepository.GetFirstOrDefault(q => q.ProductId == id);
                if (product != null)
                {
                    _unitOfWork.ProductRepository.Remove(product);
                    await _unitOfWork.SaveAsync();
                return jsonResultResponse = new JsonResultResponse() { success = true, message = "Delete product success" };
            }
            else
                {
                return jsonResultResponse = new JsonResultResponse() { success = false, message = "Delete product fail" };
            }
        }


        public async Task<PagedList<Product>> GetAllProduct(PagingParameters pagingParameters)
        {
            var result = await Task.FromResult(PagedList<Product>.GetPagedList(_unitOfWork.ProductRepository.FindAll(q => q.Amount > 0 && q.ProductStatus == true).OrderBy(o => o.ProductName), pagingParameters.PageNumber, pagingParameters.Pagesize));
            return result;
        }

        public async Task<IEnumerable<Product>> GetAllProductsByKeyword(string keyword, PagingParameters pagingParameters)
        {
            var result = await Task.FromResult(PagedList<Product>.GetPagedList(_unitOfWork.ProductRepository.FindAll( q => (q.ProductName.Contains(keyword) || q.Brand.BrandName.Contains(keyword)) && q.Amount > 0 && q.ProductStatus == true), pagingParameters.PageNumber, pagingParameters.Pagesize));
            return result;
        }



        public async Task<IEnumerable<Product>> GetAllProductsByPrice(double price1, double price2, PagingParameters pagingParameters)
        {
            var result = await Task.FromResult(PagedList<Product>.GetPagedList(_unitOfWork.ProductRepository.FindAll(q => q.Price >= price1 && q.Price <= price2 && q.Amount > 0 && q.ProductStatus == true), pagingParameters.PageNumber, pagingParameters.Pagesize));
            return result;
        }

        public async Task<Product> GetProductsById(string id)
        {
                return await _unitOfWork.ProductRepository.GetFirstOrDefault(
                q => q.ProductId.Contains(id));

        }
        public async Task<IEnumerable<ProductByBrandIdResponse>> GetProductByBandId(string id)
        {
            List<Product> listDB = (await _unitOfWork.ProductRepository.GetAll(
                q => q.BrandId.Contains(id))).ToList();
            List<ProductByBrandIdResponse> listResponse = new List<ProductByBrandIdResponse>();
            //For custom
            foreach (var product in listDB)
            {
                var creator = await _unitOfWork.ProductRepository.GetFirstOrDefault(
                    q => q.BrandId == product.BrandId, "Brand");
                listResponse.Add(new ProductByBrandIdResponse()
                {
                    ProductId = product.ProductId,
                    BrandName = creator.Brand.BrandName,
                    ProductName = product.ProductName,
                    Description = product.Description,
                    Amount = product.Amount,
                    Price = product.Price,
                    ProductImage = product.ProductImage,
                    ProductStatus = product.ProductStatus

                });
            }
            return listResponse;
        }

    }
}
