﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseAccess.Entities
{
    public partial class Voucher
    {
        public Voucher()
        {
            HistoryVouchers = new HashSet<HistoryVoucher>();
        }

        public string VoucherId { get; set; }
        public string OrderId { get; set; }
        public string Description { get; set; }
        public int? DiscountPercent { get; set; }
        public int? MaxPrice { get; set; }
        public string CreateDate { get; set; }
        public string ModifiedDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool? Active { get; set; }

        public virtual Order Order { get; set; }
        public virtual ICollection<HistoryVoucher> HistoryVouchers { get; set; }
    }
}
