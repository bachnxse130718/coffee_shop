﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.RequestModel
{
    public class TokenAPI
    {
        public string TokenId { get; set; }
        public string CardId { get; set; }
    }
}
