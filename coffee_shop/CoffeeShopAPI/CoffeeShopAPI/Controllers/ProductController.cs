﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System.Threading.Tasks;
using Utility.RequestModel;
using Utility.ResponseModel;

namespace CoffeeShopAPI.Controllers
{
    [Authorize]
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductService _productService;
        public ProductController(ProductService productService)
        {
            _productService = productService;
        }
        [AllowAnonymous]
        //Create new product
        [HttpPost("CreateNewProduct")]
        public async Task<IActionResult> CreateNewProduct(ProductItem productItem)
        {
            JsonResultResponse jsonResultResponse = await _productService.CreateNewProduct(productItem);
            return Ok(jsonResultResponse);
        }
        [AllowAnonymous]
        //Update product
        [HttpPut("UpdateProduct")]
        public async Task<IActionResult> UpdateProduct(string id, ProductItemUpdate productItem)
        {
            JsonResultResponse jsonResultResponse = await _productService.UpdateProduct(id, productItem);
            return Ok(jsonResultResponse);
        }
        [AllowAnonymous]
        //Remove product
        [HttpDelete("RemoveProduct")]
        public async Task<IActionResult> RemoveProduct(string id)
        {
            JsonResultResponse jsonResultResponse = await _productService.RemoveProduct(id);
            return Ok(jsonResultResponse);
        }

        [AllowAnonymous]
        //Get all product
        [HttpGet("GetAllProducts")]
        public async Task<IActionResult> GetAllProduct([FromQuery] PagingParameters pagingParameters)
        {
            var result = await _productService.GetAllProduct(pagingParameters);
            return Ok(result);
        }

        [AllowAnonymous]
        //Get product by keyword
        [HttpGet("GetAllProductsByKeyword")]
        public async Task<IActionResult> GetAllProductsByKeyword(string keyword, [FromQuery] PagingParameters pagingParameters)
        {
            var result = await _productService.GetAllProductsByKeyword(keyword, pagingParameters);
            return Ok(result);
        }

        [AllowAnonymous]
        //Get product by price
        [HttpGet("GetAllProductsByPrice")]
        public async Task<IActionResult> GetAllProductsByPrice(double price1, double price2, [FromQuery] PagingParameters pagingParameters)
        {
            var result = await _productService.GetAllProductsByPrice(price1, price2, pagingParameters);
            return Ok(result);
        }
       
        [AllowAnonymous]
        //Get product by id
        [HttpGet("GetProductsById")]
        public async Task<IActionResult> GetProductsById(string id)
        {
            var result = await _productService.GetProductsById(id);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("GetProductByBrandId")]
        public async Task<IActionResult> GetProductByBrandId(string id)
        {
            var result = await _productService.GetProductByBandId(id);
            return Ok(result);
        }
    }
}
